function [x, y] = TrajCDM(Tsup)
global Tc ParamMarche Techantillon

%calcul de x0_d et y0_d
for i = 1:1:5
    x0_d(i) = (ParamMarche(i,5) - (ParamMarche(i,3) - ParamMarche(i,1)) * cosh(Tsup/Tc) - ParamMarche(i,1)) / (Tc * sinh(Tsup/Tc)); 
    y0_d(i) = (ParamMarche(i,6) - (ParamMarche(i,4) - ParamMarche(i,2)) * cosh(Tsup/Tc) - ParamMarche(i,2)) / (Tc * sinh(Tsup/Tc));
end

   for i = 1:1:5
       j=1;
      for t = 0:Techantillon:Tsup
    
x(j,i) = (ParamMarche(i,3) - ParamMarche(i,1)) * cosh(t/Tc) + Tc * x0_d(1,i) * sinh(t/Tc) + ParamMarche(i,1);
y(j,i) = (ParamMarche(i,4) - ParamMarche(i,2)) * cosh(t/Tc) + Tc * y0_d(1,i) * sinh(t/Tc) + ParamMarche(i,2);
j = j + 1;

      end
  end
end
