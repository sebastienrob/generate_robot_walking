clear all
close all
clc

%PARAMETRES
global Tc ParamMarche Techantillon
Techantillon = 10^-3;
Zc = 0.8;
kx = 0.01;
ky = 0.01;
g = 9.81;
Tc = sqrt(Zc/g);
sx = 0.3;
sy = 0.2;
Tsup = 0.05;

%PLANIFICATION marche avec px//py//x(0)//y(0)//x(Tsup)//y(Tsup)
ParamMarche = zeros(5,6);
%px->zmp en x
ParamMarche(3,1) = sx;
ParamMarche(4,1) = sx + sx;
ParamMarche(5,1) = sx + sx +sx;
%py-->zmp en y
ParamMarche(2,2) = sy;
ParamMarche(4,2) = sy;
%x(0)-->posInit en x
ParamMarche(3,3) = sx;
ParamMarche(4,3) = sx + sx;
ParamMarche(5,3) = sx + sx +sx;
%y(0)--> posInit en y
ParamMarche(2,4) = sy;
ParamMarche(4,4) = sy;
%x(Tsup) -->posFinal en x
ParamMarche(2,5) = sx;
ParamMarche(3,5) = sx + sx;
ParamMarche(4,5) = sx + sx + sx;
ParamMarche(5,5) = sx + sx + sx + sx;
%y(Tsup)-->posFinal en y
ParamMarche(1,6) = sy;
ParamMarche(2,6) = 0;
ParamMarche(3,6) = sy;
ParamMarche(4,6) = 0;
ParamMarche(5,6) = sy;

%CALCUL de la trajectoire de la CDM
[x, y] = TrajCDM(Tsup);
%TRACER graphique
for i=1:1:5
   hold on
   plot(x(:,i),y(:,i));
end
